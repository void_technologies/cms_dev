<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>CMS</title>
		<link rel="stylesheet" href="css/bootstrap-3.3.6-dist/css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/style.css" >

	</head>
	<body ng-app="cmsApp">
		<header ng-hide="!currentUser">
			<div ng-include src="'app/components/header/header-view.html'"></div>
		</header>
		<div class="container">
			<div ui-view></div>
		</div>

		<!-- Application Dependencies -->
		<script src="js/libs/jquery/jquery-1.12.1.min.js"></script>
		<script src="js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="js/libs/angular/angular.js"></script>
		<script src="js/libs/angular/angular-ui-router.js"></script>
		<script src="js/libs/angular/satellizer.js"></script>
		<script src="js/libs/angular/angular-messages.js"></script>
		<script src="js/libs/angular/ui-bootstrap-tpls-1.2.4.min.js"></script>
		<script src="js/libs/angular/angular-animate.js"></script>


		<!-- Application Module -->
		<script src="app/app.js"></script>
		<script src="app/appconfig.js"></script>

		<!-- Application Services -->
		<script src="app/services/api-service.js"></script>

		<!-- Application Router -->
		<!-- <script src="app/routes.js"></script> -->

		<!-- Application Models -->
		<script src="app/models/user-model.js"></script>
		<script src="app/models/offer-model.js"></script>
		<!-- <script src="app/models/category-model.js"></script> -->

		<!--                       Application Components                              -->
		<!-- Header -->
		<script src="app/components/header/header-config.js"></script>
		<script src="app/components/header/header-ctrl.js"></script>

		<!-- Login -->
		<script src="app/components/login/login-config.js"></script>
		<script src="app/components/login/login-ctrl.js"></script>



		<!--                                 Tables                                      -->
		<!-- Offer table -->
		<script src="app/components/tables/offers/offers-config.js"></script>
		<script src="app/components/tables/offers/offers-ctrl.js"></script>



		<!-- User Table -->
		<script src="app/components/tables/users/users-config.js"></script>
		<script src="app/components/tables/users/user-ctrl.js"></script>

		<!-- Modals -->
		<script src="app/components/tables/offers/create/offer_createCtrl.js"></script>
		<script src="app/components/tables/offers/delete/offer_deleteCtrl.js"></script>
		<script src="app/components/tables/offers/update/offer_updateCtrl.js"></script>
	</body>
</html>
