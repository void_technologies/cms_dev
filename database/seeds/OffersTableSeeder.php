<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use CMS\Http\Models\Entities\Offer;
class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		DB::table('offers')->delete();

		$offers = array(
			[
				'id' => 77,
				'name' => 'Adidas',
				'highlight' => '15% de descuento',
				'sub_highlight' => 'Martes de Plaza (8, 15 & 22)',
				'content' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.',
				'address' => 'Plaza las Americas',
				'terms' => 'No se puede combinar con otras promociones. Algunas restricciones aplican.',
				'phone' => '787-522-1724',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/adidas.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '15%',
				'category_id' => 2,
				'published' => true
			],
			[
				'id' => 78,
				'name' => '5.7.9',
				'highlight' => '15% De Descuento',
				'sub_highlight' => 'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '15% descuento en una sola pieza a precio regular.',
				'address' => 'Plaza las americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.  No es valido para mercanci­a separada en layaway.',
				'phone' => '787-753-0840',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/579.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '15%',
				'category_id' => 2,
				'published' => true
			],
			[
				'id' =>79,
				'name' =>'@night',
				'highlight' =>' 10% De Descuento',
				'sub_highlight' =>'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' =>'10% de descuento en toda la mercancia.',
				'address' =>'Plaza Las Americas',
				'terms' =>'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.',
				'phone' =>'787-758-3723',
				'website' => '',
				'image' =>'http://apps.popular.com/offers/images/uploads/joyeria.jpg',
				'expiration_date' =>'2015-12-22',
				'percentage' =>'10%',
				'category_id' =>3,
				'published' => true
			],
			[
				'id' =>80,
				'name' =>'Appleberry',
				'highlight' =>'Grandes Ahorros',
				'sub_highlight' =>'Martes de Plaza (Diciembre 8, 15 & 22)',
				'content' =>'Compra un wallet por $20 (valorado en $24 a precio regular) y un screen protector por $5 (valorado en $7.95 a precio regular). Con la compra del wallet  pueden hacer el upgrade al Tempered Glass por $10 (valorado en $24.95 a precio regular).',
				'address' =>'Plaza Las Americas',
				'terms' =>'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.',
				'phone' =>'787-725-6419',
				'website' => '',
				'image' =>'http://apps.popular.com/offers/images/uploads/appleberry.jpg',
				'expiration_date' =>'2015-12-22',
				'percentage' =>'',
				'category_id' =>4,
				'published' => true
			],
			[
				'id' => 81,
				'name' => 'Arte Religioso',
				'highlight' => '15% De Descuento',
				'sub_highlight' => 'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '15% de descuento con la compra de $50 o mas.',
				'address' => 'Plaza Las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.No aplica en trabajos de imprenta, ni artesan­ales.',
				'phone' => '787-771-9596',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/artereligioso.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '15%',
				'category_id' => 5,
				'published' => true
			],
			[
				'id' => 82,
				'name' => 'Bakers',
				'highlight' => '20% De Descuento',
				'sub_highlight' => 'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '20% de descuento en mercanci­a a precio regular. No aplica con otras ofertas.  Precios regulares desde $19.99-$49.99.',
				'address' => 'Plaza Las Americas y Plaza Del Caribe',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.No aplica con otras ofertas.  Precios regulares desde $19.99-$49.99.',
				'phone' => '787-751-6719',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/bakers.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '20%',
				'category_id' => 4,
				'published' => true
			],
			[
				'id' => 83,
				'name' => 'Banana Republic',
				'highlight' => '10% De Descuento',
				'sub_highlight' => '  Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '10 % de descuento en mercanci­a a precio regular.',
				'address' => 'Plaza las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular. Oferta no es valida en li­nea, para las mercanci­as enviadas en comercios no participantes o en Banana Republic Factory Stores. Aplica a mercanci­a Unicamente y no se puede aplicar al valor de Giftcards, certificados de regalo, embalaje, alteraciones , y gastos de envi­o e impuestos aplicables.No se puede combinar con otras ofertas, incluyendo pero no limitado a \"senior discount\", descuento empleado, Shopcards, descuentos de  dolar, etc. Exclusiones: monograma, Patrimonio, el 100% de la cachemere, Coleccion de Hombres \"Performanceâ€, Blazers de hombres, Nueva Mercanci­a, Designer Collections, Mad Men Collection y mercanci­a de otras marcas: (es decir, Spanx, Adidas, etc.).',
				'phone' => '787-296-7100',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/bananarepublic.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '',
				'category_id' => 6,
				'published' => true
			],
			[
				'id' => 84,
				'name' => 'Bora Bora',
				'highlight' => ' 25% De Descuento',
				'sub_highlight' => '   Martes de Plaza (Diciembre 8, 15 & 22)',
				'content' => '25% de descuento en ropa de mujer a precio regular.',
				'address' => 'Plaza Las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.No aplica con otras ofertas.',
				'phone' => '787-753-7305',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/borabora.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '25%',
				'category_id' => 2,
				'published' => true
			],
			[
				'id' => 85,
				'name' => 'Bostonian',
				'highlight' => '20% De Descuento',
				'sub_highlight' => '  Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '20 % discount.',
				'address' => 'Plaza Las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.Excluye mercanci­a a precio regular \"Unstructured\", Clarks Originales. No aplica con otros cupones o bonos de descuento, en compras anteriores o tarjetas de regalo.',
				'phone' => '787-771-9467',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/bostonian.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '20%',
				'category_id' => 3,
				'published' => true
			],
			[
				'id' => 86,
				'name' => 'Brooks Brothers',
				'highlight' => '15% De Descuento',
				'sub_highlight' => '  Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '15% de descuento en la compra de mercanci­a a precio regular, Unicamente en la tienda.',
				'address' => 'Plaza Las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.Ciertas restricciones aplican. No es valido para compras online ni por telefono. No se puede combinar con descuentos de estudiantes, militar o membresi­as coporativas. Excluye Black Fleece, Charitable Items, Made to Measure, Special Order Clothing, Resort Merchandise y alguna seleccion de estilos de zapatos. El descuento no puede ser aplicado a impuestos, manejo y franqueo, monogramas, grabados, alteraciones o personalizaciones. No es valido para compras previas o en compras de tarjetas de regalo. En caso de devolucion de mercanci­a, el valor del descuento no es rembolsable o no se acreditara a la cuenta.',
				'phone' => '787-767-1800',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/brooksbrothers.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '15%',
				'category_id' => 3,
				'published' => true
			],
			[
				'id' => 87,
				'name' => 'Campo Marzio',
				'highlight' => '10 % De Descuento',
				'sub_highlight' => 'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '10 % de descuento en compras a precio regular.',
				'address' => 'Plaza las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular.No se puede combinar con otras ofertas.',
				'phone' => '787-294-0442',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/campoMarzio.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '10%',
				'category_id' => 3,
				'published' => true
			],
			[
				'id' => 88,
				'name' => 'Claires',
				'highlight' => '20 % De Descuento',
				'sub_highlight' => 'Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '20 % de descuento en la compra final.',
				'address' => 'Plaza del Caribe & Plaza las Americas',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular. No es valido en tarjetas de regalo, compras previas y otras ofertas. No se es valido para recibir efectivo ni credito.',
				'phone' => '787-767-3747',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/claires.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '20%',
				'category_id' => 2,
				'published' => true
			],
			[
				'id' => 89,
				'name' => 'Clarks',
				'highlight' => ' 20 % De Descuento',
				'sub_highlight' => '  Martes de Plaza  (Diciembre 8, 15 & 22)',
				'content' => '20 % de descuento en su compra.',
				'address' => 'Plaza Las Americas y Plaza Del Caribe',
				'terms' => 'Oferta valida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de Credito o Debito ATH de Banco Popular. Excluye mercanci­a a precio regular \"Unstructured\" y Clarks Originales. No se puede combinar con otros cupones de descuento o bonos de descuento. No es valido en compras anteriores o tarjetas de regalo.',
				'phone' => '787-281-0856',
				'website' => '',
				'image' => 'http://apps.popular.com/offers/images/uploads/clarks.jpg',
				'expiration_date' => '2015-12-22',
				'percentage' => '20%',
				'category_id' => 4,
				'published' => true
			]
		);

		foreach ($offers as $offer) {
			Offer::create($offer);
		}

		Model::reguard();
    }
}
