<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

		$this->call(RolesTableSeeder::class);
        $this->call(UserTableSeed::class);
        $this->call(CategoryTableSeeder::class);
		$this->call(OffersTableSeeder::class);

        Model::reguard();
    }
}
