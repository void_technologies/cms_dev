<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use CMS\Http\Models\Entities\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		DB::table('roles')->delete();

		$roles = array(
			[
				'id' => 1,
				'name' => 'Administrator'
			],
			[
				'id' => 2,
				'name' => 'User'
			]
		);

		foreach ($roles as $role) {
			Role::create($role);
		}

		Model::reguard();
    }
}
