<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use CMS\Http\Models\Entities\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		DB::table('categories')->delete();

		$categories = array(
			[
				'name' => 'Mas Recientes'
			],
			[
				'name' => 'Belleza y Salud'
			],
			[
				'name' => 'Deportes'
			],
			[
				'name' => 'Martes de Ahorro'
			],
			[
				'name' => 'Martes de Plaza'
			],
			[
				'name' => 'Otros'
			]
		);

		foreach ($categories as $category) {
			Category::create($category);
		}

		Model::reguard();
    }
}
