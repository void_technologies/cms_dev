<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use CMS\Http\Models\Entities\User;
class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

		DB::table('users')->delete();

		$users = array(
			['name' => 'Administrator', 'email' => 'admin@cms.com', 'password' => Hash::make('admin'), 'role_id' => 1],
			['name' => 'User', 'email' => 'user@cms.com', 'password' => Hash::make('user'), 'role_id' => 2],
		);

		foreach ($users as $user) {
			User::create($user);
		}

		Model::reguard();
    }
}
