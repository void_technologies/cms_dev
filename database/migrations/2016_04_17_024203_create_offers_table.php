<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('offers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name', 25);
          $table->string('highlight', 50);
          $table->string('sub_highlight', 80);
          $table->string('content', 500);
		  $table->string('address', 100);
		  $table->string('terms', 500);
		  $table->string('phone', 12);
          $table->string('website', 80)->nullable();
          $table->string('image', 30);
          $table->date('expiration_date');
          $table->string('percentage', 3)->nullable();
          $table->integer('category_id');
          $table->boolean('published');
          $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('offers');
  }
}
