-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: cms
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Mas Recientes'),(2,'Belleza y Salud'),(3,'Deportes'),(4,'Martes de Ahorro'),(5,'Martes de Plaza'),(6,'Otros');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `highlight` text NOT NULL,
  `sub_highlight` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `address` text NOT NULL,
  `terms` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `website` varchar(80) NOT NULL,
  `url` varchar(80) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `expiration` date NOT NULL,
  `alert_qty` int(11) NOT NULL,
  `image_name` text NOT NULL,
  `percentage` varchar(10) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (77,'Adidas ','15% de descuento.',' Martes de Plaza(8,15 & 22)','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.','Plaza las Americas','No se puede combinar con otras promociones. Algunas restricciones aplica.','787-522-1724','','',',','http://apps.popular.com/offers/images/uploads/adidas.jpg','2015-12-22',0,'adidas','15%',1),(78,'5.7.9','15% De Descuento','   Martes de Plaza  (Diciembre 8, 15 & 22)',' 15% descuento en una sola pieza a precio regular.','Plaza las americas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.  No es vÃ¡lido para mercancÃ­a separada en layaway.','787-753-0840','','',',','http://apps.popular.com/offers/images/uploads/579.jpg','2015-12-22',0,'579','15%',1),(79,'@night',' 10% De Descuento',' Martes de Plaza  (Diciembre 8, 15 & 22)','10% de descuento en toda la mercancÃ­a.','Plaza Las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.','787-758-3723','','',',','http://apps.popular.com/offers/images/uploads/joyeria.jpg','2015-12-22',0,'joyeria','10%',1),(80,'Appleberry','Grandes Ahorros','  Martes de Plaza (Diciembre 8, 15 & 22)','Compra un wallet por $20 (valorado en $24 a precio regular) y un screen protector por $5 (valorado en $7.95 a precio regular). Con la compra del wallet  pueden hacer el upgrade al Tempered Glass por $10 (valorado en $24.95 a precio regular).','Plaza Las Americas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.','787-725-6419','','',',','http://apps.popular.com/offers/images/uploads/appleberry.jpg','2015-12-22',0,'appleberry','',1),(81,'Arte Religioso','15% De Descuento ','  Martes de Plaza  (Diciembre 8, 15 & 22)','15% de descuento con la compra de $50 o mÃ¡s.','Plaza Las Americas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.No aplica en trabajos de imprenta, ni artesanÃ­as.','787-771-9596','','',',','http://apps.popular.com/offers/images/uploads/artereligioso.jpg','2015-12-22',0,'artereligioso','15%',1),(82,'Bakers','20% De Descuento','  Martes de Plaza  (Diciembre 8, 15 & 22)','20% de descuento en mercancÃ­a a precio regular. No aplica con otras ofertas.  Precios regulares desde $19.99-$49.99.','Plaza Las AmÃ©ricas y Plaza Del Caribe','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.No aplica con otras ofertas.  Precios regulares desde $19.99-$49.99.','787-751-6719','','',',','http://apps.popular.com/offers/images/uploads/bakers.jpg','2015-12-22',0,'bakers','20%',1),(83,'Banana Republic','10 % De Descuento','  Martes de Plaza  (Diciembre 8, 15 & 22)','10 % de descuento en mercancÃ­a a precio regular.','Plaza las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular. Oferta no es vÃ¡lida en lÃ­nea, para las mercancÃ­as enviadas en comercios no participantes o en Banana Republic Factory Stores. Aplica a mercancÃ­a Ãºnicamente y no se puede aplicar al valor de Giftcards, certificados de regalo, embalaje, alteraciones , y gastos de envÃ­o e impuestos aplicables.No se puede combinar con otras ofertas, incluyendo pero no limitado a \"senior discount\", descuento empleado, Shopcards, descuentos de  dÃ³lar, etc. Exclusiones: monograma, Patrimonio, el 100% de la cachemere, ColecciÃ³n de Hombres \"Performanceâ€, Blazers de hombres, Nueva MercancÃ­a, Designer Collections, Mad Men Collection y mercancÃ­a de otras marcas: (es decir, Spanx, Adidas, etc.).','787-296-7100','','',',','http://apps.popular.com/offers/images/uploads/bananarepublic.jpg','2015-12-22',0,'bananarepublic','',1),(84,'Bora Bora',' 25% De Descuento','   Martes de Plaza (Diciembre 8, 15 & 22)','25% de descuento en ropa de mujer a precio regular.','Plaza Las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.No aplica con otras ofertas.','787-753-7305','','',',','http://apps.popular.com/offers/images/uploads/borabora.jpg','2015-12-22',0,'borabora','25%',1),(85,'Bostonian','20% De Descuento','  Martes de Plaza  (Diciembre 8, 15 & 22)','20 % discount.','Plaza Las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.Excluye mercancÃ­a a precio regular \"Unstructured\", Clarks Originales. No aplica con otros cupones o bonos de descuento, en compras anteriores o tarjetas de regalo.','787-771-9467','','',',','http://apps.popular.com/offers/images/uploads/bostonian.jpg','2015-12-22',0,'bostonian','20%',1),(86,'Brooks Brothers','15% De Descuento','  Martes de Plaza  (Diciembre 8, 15 & 22)','15% de descuento en la compra de mercancÃ­a a precio regular, Ãºnicamente en la tienda.','Plaza Las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.Ciertas restricciones aplican. No es vÃ¡lido para compras online ni por telÃ©fono. No se puede combinar con descuentos de estudiantes, militar o membresÃ­as coporativas. Excluye Black Fleece, Charitable Items, Made to Measure, Special Order Clothing, Resort Merchandise y alguna selecciÃ³n de estilos de zapatos. El descuento no puede ser aplicado a impuestos, manejo y franqueo, monogramas, grabados, alteraciones o personalizaciones. No es vÃ¡lido para compras previas o en compras de tarjetas de regalo. En caso de devoluciÃ³n de mercancÃ­a, el valor del descuento no es rembolsable o no se acreditarÃ¡ a la cuenta.','787-767-1800','','',',','http://apps.popular.com/offers/images/uploads/brooksbrothers.jpg','2015-12-22',0,'brooksbrothers','15%',1),(87,'Campo Marzio','10 % De Descuento','Martes de Plaza  (Diciembre 8, 15 & 22)','10 % de descuento en compras a precio regular.','Plaza las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular.No se puede combinar con otras ofertas.','787-294-0442','','',',','http://apps.popular.com/offers/images/uploads/campoMarzio.jpg','2015-12-22',0,'campoMarzio','10%',1),(88,'Claires','20 % De Descuento','Martes de Plaza  (Diciembre 8, 15 & 22)','20 % de descuento en la compra final.','Plaza del Caribe & Plaza las AmÃ©ricas','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular. No es vÃ¡lido en tarjetas de regalo, compras previas y otras ofertas. No se es vÃ¡lido para recibir efectivo ni crÃ©dito.','787-767-3747','','',',','http://apps.popular.com/offers/images/uploads/claires.jpg','2015-12-22',0,'claires','20%',1),(89,'Clarks',' 20 % De Descuento','  Martes de Plaza  (Diciembre 8, 15 & 22)','20 % de descuento en su compra.','Plaza Las AmÃ©ricas y Plaza Del Caribe','Oferta vÃ¡lida durante los martes, 8, 15 y 22 de diciembre al pagar con tu Tarjeta de CrÃ©dito o DÃ©bito ATHÂ® de Banco Popular. Excluye mercancÃ­a a precio regular \"Unstructured\" y Clarks Originales. No se puede combinar con otros cupones de descuento o bonos de descuento. No es vÃ¡lido en compras anteriores o tarjetas de regalo.','787-281-0856','','',',','http://apps.popular.com/offers/images/uploads/clarks.jpg','2015-12-22',0,'clarks','20%',1),(90,'TestOffer','Something','Somteh','sadkajsdkjaslkdjkasjdl','asdasdasd','none','787-787-7979','none','none','','','2016-09-09',34,'none','20%',6),(91,'Action','some','some','asddasdsa','asdasd','none','898-898-8989','none','none','','','2020-02-03',5,'none','45%',3),(93,'Funciono','rrerer','asdasdas','dasdasds','asdasdas','noone','asdasdas','asdasd','asdasd','','','2020-02-03',123,'none','50%',3),(94,'Testing Create','asda','asdasd','asdasd','asdasd','asdas','787-787-7777','asdas','asda','','','2016-04-12',0,'asdas','12%',0),(95,'Testing new generic api','asdasda','asdasda','asdasdasd','asdasd','asdas','787-787-7979','asdas','asdas','','','2016-09-09',0,'asdas','asdsa',0);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(13) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator'),(2,'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrator','admin@cms.com','$2y$10$zUxgPa2GO4WAOE.KhpSXhuA/QBd/w84ySsPXToc7NZ3sZd5gjosDG',1,NULL,'2016-03-08 07:41:54','2016-03-08 07:41:54'),(2,'User','user@cms.com','$2y$10$zavdkJQkiuL0caL3eZAt8erQPZLZ3Y7OniMHKtE7A02NUL9QpgAvO',2,NULL,'2016-03-08 07:41:54','2016-03-08 07:41:54');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cms'
--

--
-- Dumping routines for database 'cms'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-21  2:08:33
