<?php
	use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return File::get(public_path().'/index.html');
});

Route::group(['middleware' => 'cors', 'prefix' => 'api'], function () {
	Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
	Route::post('authenticate', 'AuthenticateController@authenticate');
	Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

	// Users Routing
	Route::resource('users', 'UserController');

	// Offers Routing
	Route::resource('offers', 'OfferController');

	//Categories Routing
	Route::resource('categories', 'CategoryController');

	Route::resource('photo', 'PhotoController');

	Event::listen('illuminate.query', function ($query) {
		Log::info($query.PHP_EOL);
	});
});
