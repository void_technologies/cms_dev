<?php
namespace CMS\Http\Controllers;

use Illuminate\Http\Request;
use CMS\Http\Models\Entities\User;
use CMS\Http\Models\UserModel;
use CMS\Http\Controllers\Controller;
use JWTAuth;

class UserController extends Controller {

	private static $fieldsAllowed = array('user_id', 'name' ,'email', 'password', 'role_id', 'created_at', 'updated_at');

	public function __construct() {
		$this->middleware('jwt.auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = UserModel::getUsers()->toArray();
		return json_encode($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$queryParam = array();
		foreach (self::$fieldsAllowed as $field) {
			if ($request->has($field)) {
					$queryParam[$field] = $request->input($field);
			}
		}
		$new_user_id = UserModel::create($queryParam);
		$response = array("error" => null, "newuser" => $new_user_id);
		return response()->json($response)->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id) {
        $user = UserModel::getById($user_id);
		return json_encode($user);
    }

    /** TODO -- check when sending real data
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id) {
        UserModel::update($request->all(), $user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id) {
        UserModel::delete($user_id);
    }
}
