<?php
namespace CMS\Http\Controllers;
/*********************************************
 * TODO - Manage Errors - TODO
 *		- Return Responses of status
 * TODO - Document Code - TODO
***********************************************/
use Illuminate\Http\Request;
use CMS\Http\Models\OfferModel;
use CMS\Http\Controllers\Controller;
use CMS\Http\Helpers\Constants;
use Illuminate\Support\Facades\Log;

class OfferController extends Controller {

	private static $fieldsAllowed = array(
		'id', 'name', 'highlight', 'sub_highlight',	'content', 'address',
		'terms', 'phone', 'website', 'image', 'expiration_date',
		'percentage', 'category_id'
	);

	// Operations Constants for Helpers method
	const STORE  = 1;
	const UPDATE = 2;

	public function __construct() {
		$this->middleware('jwt.auth', ['except' => ['index']]);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$offers = OfferModel::getOffers();
		return json_encode($offers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$new_offer = null;
		$offer = array();
		$error = null;
		$httpStatusCode = Constants::NO_CONTENT;

		if ($request->has("data")) {
			$offer = $request->input("data");
			$error = self::canStoreModel($offer, self::STORE);
			if ($error == null) {
				foreach (self::$fieldsAllowed as $field) {
					if (isset($offer[$field])) {
						$query[$field] = $offer[$field];
					}
				}
				$new_offer = OfferModel::create($query);
				if (empty($new_offer)) {
					$error = "Internal Server Error";
					$httpStatusCode = Constants::INTERNAL_ERROR;
				}
				else {
					$httpStatusCode = Constants::CREATED;
				}
			}
			else {
				$httpStatusCode = Constants::BAD_REQUEST;
			}
		}
		else {
			$httpStatusCode = Constants::BAD_REQUEST;
		}
		$response = array("error" => $error, "status" => $httpStatusCode, "id" => $new_offer["id"]);
		return response()->json($response)->setStatusCode($httpStatusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $offer_id
     * @return \Illuminate\Http\Response
     */
    public function show($offer_id) {
        $offer = OfferModel::getById($offer_id);
		return json_encode($offer);
    }

    /** TODO -- after implementing front end
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $offer_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $offer_id) {
        OfferModel::update($request->all(), $offer_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $offer_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($offer_id) {
		$error = null;
		$httpStatusCode = null;
		$error = OfferModel::delete($offer_id);
		if (empty($error)) {
			$httpStatusCode = Constants::OK;
		}
		else {
			$httpStatusCode = Constants::BAD_REQUEST;
		}
		$response = array("error" => $error, "status" => $httpStatusCode);
		return response()->json($response)->setStatusCode($httpStatusCode);
    }

	// Helpers Method
	private function canStoreModel($offer, $operation) {
		$error = null;
		foreach (self::$fieldsAllowed as $field) {
			if (empty($offer[$field])) {
				if (! self::isFieldNullable($field, $operation)) {
					$error = "$field field is required";
					break;
				}
			}
		}
		return $error;
	}

	private function isFieldNullable ($field, $operation) {
		switch ($operation) {
			case self::STORE:
				return ($field == 'website'
						|| $field == 'percentage'
						|| $field == 'id'
					);
			case self::UPDATE:
				return ($field == 'website'
						|| $field == 'percentage'
					);
			default:
				return false;
		}
	}
}
