<?php
namespace CMS\Http\Controllers;

use Illuminate\Http\Request;
use CMS\Http\Models\CategoryModel;
use CMS\Http\Models\OffersCategoryModel_VIEW;
use CMS\Http\Controllers\Controller;
use DateTime;

use Illuminate\Support\Facades\Log;

class CategoryController extends Controller {

	const MOST_RECENT_LIMIT = 10;

	public function __construct() {
		$this->middleware('jwt.auth', ["except" => ["index"]]);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
		$categories = null;
		if ($request->has('with')) {
			$param = $request->get("with");
			if ($param == 'offers') {
				$categories = self::composeMobileJSON();
			 }
			else {
				$categories = "Error";
			}
		}
		else {
			$categories = CategoryModel::getCategories();
		}
		return json_encode($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $new_category_id = CategoryModel::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */
    public function show($category_id) {
        $category = CategoryModel::getById($category_id);
		return json_encode($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id) {
        // TODO -- after implementing front end
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id) {
        CategoryModel::delete($category_id);
    }

	private function composeMobileJSON() {
		$offersCategories = OffersCategoryModel_VIEW::getOffersCategories();
		$count = count($offersCategories);

		$categories = array();

		$most_recents = array(
			"cat_id" => 1,
			"cat_title" => "Most Recents",
			"offers" => array()
		);

		for ($i = 0; $i < $count; $i++) {
			$data = $offersCategories[$i];
			$category_id = $data["category_id"];
			$category_name = $data["cat_name"];

			$exist = false;
			foreach ($categories as $c) {
				if (isset($c["cat_id"])) {
					if ($c["cat_id"] == $category_id) {
						$exist = true;
					}
				}
			}

			if (! $exist) {
				$categories[] = array(
					"cat_id" => $category_id,
					"cat_title" => $category_name,
					"offers" => array()
				);
			}

			for ($j = 0; $j < count($categories); $j++) {
				$cat = $categories[$j];
				if ($cat["cat_id"] == $category_id) {
					$data["image"] = "http://offercms.azurewebsites.net/".$data["image"];
					$categories[$j]["offers"][] = $data;
					if (self::isRecentOffer($data["created_at"])) {
						$most_recents["offers"][] = $data;
					}
				}
			}
		}
		$categories[] = $most_recents;
		return $categories;
	}

	private function isRecentOffer($date) {
		$current_date = new DateTime();
		$past_date = new DateTime($date);
		// Log::info($past_date->diff($current_date)->format("%d").PHP_EOL);
		return $past_date->diff($current_date)->format("%d") <= self::MOST_RECENT_LIMIT;
	}
}
