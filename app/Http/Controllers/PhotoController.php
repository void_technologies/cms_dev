<?php
namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CMS\Http\Helpers\Constants;

use Illuminate\Support\Facades\Log;

class PhotoController extends Controller {

	const LIMIT_SIZE = 100000;
	private static $allowedPhotoTypes = array('image/jpeg','image/jpg','image/gif','image/png');

	public function store(Request $request) {
		$error = null;
		$path = null;
		$httpStatusCode = Constants::BAD_REQUEST;
		if (!$request->file('photo')->isValid()) {
			$error  = $request->file('photo')->getErrorMessage();
		}
		else if(!in_array($request->file('photo')->getClientMimeType(), self::$allowedPhotoTypes)) {
			$error  = "Picture extension must be PNG, JPG or GIF. Please try again.";
		}
		else if($request->file('photo')->getClientSize() > self::LIMIT_SIZE) {
			$error  = "Picture size can't be larger than 100KB. Your file size is: {$request->file ( 'photo' )->getClientSize()} B. Please try again.";
		}
		else {
			$file = $request->file("photo");
			$photoFor = $request->input("photofor");
			$id = $request->input("photoid");
			if (!empty($id) && !empty($photoFor)) {
				$path = self::getPhotoPath($photoFor, $id);
				$file->move($path, self::getPhotoName($photoFor));

				$httpStatusCode = Constants::OK;
			} else {
				$error = "Bad request ".$request->input("photoid");
			}
		}
		$response = array("error" => $error, "image_path" => $path);
		return response()->json($response)->setStatusCode($httpStatusCode);
	}

	private function getPhotoPath($photoFor, $id) {
		$photoPath = "storage";
		switch ($photoFor) {
			case "offers":
				if (! file_exists("storage/offers")) {
					mkdir("storage/offers");
				}
				if (! file_exists("storage/offers/{$id}")) {
					mkdir("storage/offers/{$id}", 0755);
				}
				$photoPath .= "/offers/{$id}/";
				break;
			case "users":
				if (! file_exists("storage/users")) {
					mkdir("storage/users");
				}
				if (! file_exists("storage/users/{$id}")) {
					mkdir("storage/users/{$id}", 0755);
				}
				$photoPath .= "storage/users/{$id}/";
		}
		return $photoPath;
	}

	private function getPhotoName($photoFor) {
		$newname = "default";
		switch ($photoFor) {
			case "offers":
				$newname = "offer01.jpg";
				break;
			case "users":
				$newname = "user01.jpg";
				break;
		}
		return $newname;
	}
}
?>
