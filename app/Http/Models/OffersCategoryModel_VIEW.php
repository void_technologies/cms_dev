<?php
	namespace CMS\Http\Models;

	use CMS\Http\Models\Entities\OffersCategory_VIEW;
	use Illuminate\Support\Facades\Log;

	class OffersCategoryModel_VIEW {

		public static function getOffersCategories () {
			$offersCategories = OffersCategory_VIEW::all();
			return $offersCategories->toArray();
		}
	}
?>
