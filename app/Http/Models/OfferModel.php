<?php
namespace CMS\Http\Models;

use CMS\Http\Models\Entities\Offer;
use Illuminate\Support\Facades\Log;

class OfferModel {

	const IMAGE_PATH = "storage/offers/";
	// Create new offer in Offer Table
	public static function create ($data) {
		$offer_id = Offer::create($data);
		// Log::info(json_encode($offer_id).PHP_EOL);
		return $offer_id;
	}

	// Read offer from Offer Table
	public static function getById ($offer_id) {
		$offer = Offer::where('id', $offer_id)->get();
		return $offer->toArray();
	}

	// Update an Offer from Offer Table
	public static function update ($data, $offer_id) {
		Offer::where('id', $offer_id)->update($data);
	}

	// Delete an offer from Offer Table
	public static function delete ($offer_id) {
		$message = null;
		if (file_exists(self::IMAGE_PATH.$offer_id))
			if (unlink(self::IMAGE_PATH.$offer_id."/offer01.jpg"))
				if (rmdir(self::IMAGE_PATH.$offer_id))
					Offer::where('id', $offer_id)->delete();
				else
					$message = "Failed on Offer Directory with id -> ($offer_id)";
			else
				$message = "Failed on picture deletion";
		else
			Offer::where('id', $offer_id)->delete();
		return $message;
	}

	// Retrieve all the offers from Offer Table
	public static function getOffers () {
		$offers = Offer::all();
		return $offers;
	}
}
