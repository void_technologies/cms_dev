<?php
namespace CMS\Http\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {
	/**
	* The dabase table used by the model
	*
	* @var string
	*/
	protected $table = 'offers';

	protected $fillable = array(
		'name',
		'highlight',
		'sub_highlight',
		'content',
		'address',
		'terms',
		'phone',
		'website',
		'url',
		'image',
		'expiration_date',
		'alert_qty',
		'percentage',
		'category_id',
		'published',
		'created_at',
		'updated_at'
	);

	public function category() {
		return $this->belongsTo("CMS\Http\Models\Entities\Category", "category_id", "category_id");
	}
}
