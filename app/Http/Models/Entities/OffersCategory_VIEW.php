<?php
	namespace CMS\Http\Models\Entities;

	use Illuminate\Database\Eloquent\Model;

	class OffersCategory_VIEW extends Model {
	  /**
	   * The dabase table used by the model
	   *
	   * @var string
	   */
	   protected $table = 'offers_with_category';
}
