<?php
namespace CMS\Http\Models\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

	use Authenticatable, Authorizable, CanResetPassword;
    /**
     * The dabase table used by the model
     *
     * @var string
     */
     protected $table = 'users';

	 /**
	  * The attributes that are mass assignable.
	  *
	  * @var array
	  */
	 protected $fillable = ['name', 'email', 'password', 'role_id'];

	 /**
	  * The attributes excluded from the model's JSON form.
	  *
	  * @var array
	  */
	 protected $hidden = ['password', 'remember_token'];

    //  // Creates a Relationship with Roles table
    //  // One user has One Role
    //  public function toRoles() {
    //    return $this->hasOne('CMS\Http\Models\Entities\Role', 'role_id');
    //  }
}
