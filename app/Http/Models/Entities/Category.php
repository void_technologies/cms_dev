<?php
namespace CMS\Http\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
  /**
   * The dabase table used by the model
   *
   * @var string
   */
   protected $table = 'categories';

   public $timestamps = false;
}
