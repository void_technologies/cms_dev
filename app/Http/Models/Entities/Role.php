<?php
namespace CMS\Http\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
  /**
   * The dabase table used by the model
   *
   * @var string
   */
   protected $table = 'roles';

   public $timestamps = false;
}
