<?php
namespace CMS\Http\Models;

use CMS\Http\Models\Entities\Category;
use Illuminate\Support\Facades\Log;

class CategoryModel {

	// Creates a new category on Category Table
	public static function create ($data) {
		$category_id = Category::insertGetId($data);
		return $category_id;
	}

	// Reads a Category from Category Table
	public static function getById ($category_id) {
		$category = Category::where('category_id', $category_id)->get();
		return $category->toArray();
	}

	// Updates a specific Category from category table
	public static function update ($data, $category_id) {
		Category::where('category_id', $category_id)->update($data);
	}

	// Deletes a Category from Category Table
	public static function delete ($category_id) {
		Category::where('category_id', $category_id)->delete();
	}

	// Retrieves all Categories from Category Table
	public static function getCategories () {
		$categories = Category::all();
		return $categories;
	}
}
