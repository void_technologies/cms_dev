<?php

namespace CMS\Http\Models;

use CMS\Http\Models\Entities\Role;
use Illuminate\Support\Facades\Log;
class RoleModel {

	public static function create ($data) {
		$role_id = Role::insertGetId($data);
		return $role_id;
	}

}
