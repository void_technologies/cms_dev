<?php
namespace CMS\Http\Models;

use CMS\Http\Models\Entities\User;
use Illuminate\Support\Facades\Log;

class UserModel {

	// Creates a new user on the User table in database
	public static function create ($data) {
		if (!empty($data["password"])) {
			$data["password"] = bcrypt($data["password"]);
		}
		$user_id = User::insertGetId($data);
		return $user_id;
	}

	// Retrieve a Specific user by its ID
	// READ METHOD on CRUD
	public static function getById ($user_id) {
      	$user = User::where('id', $user_id)->get();
		return $user->toArray();
    }

	// Updates an existing user from User Table in Database
	public static function update ($data, $user_id) {
		User::where('id', $user_id)->update($data);
	}

	// Deletes an Existing User from User Table in Database
	public static function delete ($user_id) {
		User::where('id', $user_id)->delete();
	}

	// Retrieves a Specific user by its email
	public static function getByEmail ($email) {
		$user = User::where('id', $email)->get();
		return $user->toArray();
	}

	// Retrieve all users on DB without its password
	public static function getUsers () {
		$users = User::select('id', 'name', 'email', 'role_id')
				->get();
		return $users;
	}

	// Retrieves 1 wheter an email is found
	// Will be used to validate users
	public static function getEmailCount ($email) {
		$count = User::where('email', $email)->count();
		return $count;
	}
}
