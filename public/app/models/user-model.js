(function () {
	'use strict'

	angular
		.module('cmsApp')
		.factory('UserModel', UserModel);

	UserModel.$inject = ['dataservice']

	function UserModel (dataservice) {
		var user = this;
		var helpers = {};
		user.List = new Collection();

		function Model () {
			this.id      = undefined; // expects a number
			this.name    = undefined; // expects a string
			this.email   = undefined; // expects a string
			this.role_id = undefined; // expects a number
		}

		function Collection () {
			this.users = undefined; // expects a collection of users
		}

		function init () {
			user.List.users = [];

			dataservice.all("users")
				.then(function (response) {
					user.List.users = response.data;
				})
				.catch(function (error) {
					console.log(error);
				});
		}

		/************************************
			@desc Model Helper Operations
		************************************/
		function isUserAvailable (collection, email) {
			var available = true;
			for (var currentUser = 0; currentUser < collection.length; currentUser++) {
				if (collection[currentUser]['email'] == email) {
					available = false;
				}
			}
			return available;
		}
		helpers = {
			getRoleName: function (role_id) { return (role_id == 1) ? 'Administrator' : 'User';	},
			canAddUser: isUserAvailable,
		};

		return {
			init       : init,
			Model      : Model,
			Collection : user.List,
			Helpers    : helpers,
		};
	}
})();
