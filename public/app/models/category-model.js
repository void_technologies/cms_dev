(function () {
	"use strict"

	angular
		.module("cmsApp")
		.factory("CategoryModel", CategoryModel);

	CategoryModel.$inject = ["dataservice"];

	function CategoryModel (dataservice) {
		var category = this;
		category.List = new Collection();

		return {
			init       : init,
			Model      : Model,
			Collection : category.List,
		};

		function Model () {
			this.id   = undefined; // expects a number
			this.name = undefined; // expects a string
		}

		function Collection () {
			this.categories = undefined; // expects a collection of categories
		}

		function init () {
			category.List.categories = [];

			dataservice.all("categories")
				.then(function (response) {
					category.List.categories = response.data;
				})
				.catch(function (error) {
					console.log(error);
				});
		}
	}
})();
