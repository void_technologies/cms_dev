(function () {
	'use strict'

	angular
		.module('cmsApp')
		.factory('OfferModel', OfferModel);

	OfferModel.$inject = ['dataservice'];

	function OfferModel (dataservice) {
		var offer = this;
		offer.List = new Collection();
		return {
			init       : init,
			Model      : Model,
			Collection : offer.List,
		};
		/**
			Model -
		**/
		function Model () {
			this.id              = undefined;	// expects a number
			this.name            = undefined; // expects a string
			this.highlight       = undefined; // expects a string
			this.sub_highlight   = undefined; // expects a string
			this.content         = undefined; // expects a string
			this.address         = undefined; // expects a string
			this.terms           = undefined; // expects a string
			this.phone           = undefined; // expects a string
			this.website         = undefined; // expects a string
			this.image           = undefined; // expects a string
			this.expiration_date = undefined; // expects a date
			this.precentage      = undefined; // expects a string
			this.category_id     = undefined; // expects a number
			this.published       = undefined; // expects a letter
			this.created_at      = undefined; // expects a date
			this.updated_at      = undefined; // expects a date
		}

		function Collection () {
			this.offers = []; // expects a collection of offers
		}

		function init () {
			offer.List.offers = [];
			dataservice.all("offers")
				.then(function (response) {
					offer.List.offers = response.data;
				})
				.catch(function (error) {
					console.log(error);
				});
		}
	}
})();
