(function () {
	'use strict'

	angular
		.module('cmsApp')
		.component('headerNav',{
			templateUrl: '../app/components/layout/header-template.html',
			controller: headerNavController,
			controllerAs: 'vm',
			bindings: {
				isloggedin: '=',
				onLogout: '&'
			}
		});

		headerNavController.$inject = [];

		function headerNavController () {
			var vm = this;

			activate();

			function activate () {

			}
		}
})();
