(function () {
	'use strict'

	angular
		.module('cmsApp')
		.config(LoginConfig);

		LoginConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

		function LoginConfig ($stateProvider, $urlRouterProvider) {
			$stateProvider
				.state('auth', {
					url: '/auth',
					data: {
						permissions: {
							except: ['isloggedin'],
							redirectTo: 'offers'
						}
					},
					templateUrl: '../app/components/login/login-view.html',
					controller: 'LoginCtrl',
					controllerAs: 'auth'
				});
		}
})();
