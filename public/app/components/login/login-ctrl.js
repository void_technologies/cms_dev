(function () {
	'use strict'

	angular
		.module('cmsApp')
		.controller('LoginCtrl', LoginCtrl);

		LoginCtrl.$inject = ['$auth', '$state', '$http', '$rootScope', 'UserModel'];

		function LoginCtrl ($auth, $state, $http, $rootScope, UserModel) {
			var vm = this;

			vm.loginError = false;
			vm.loginErrorMsg;

			vm.login = function () {
				var credentials = {
					email: vm.email,
					password: vm.password
				};
				$auth.login(credentials)
					.then(function () {
						$http.get('/api/authenticate/user')
							.success(function (response) {
								var user = JSON.stringify(response.user);
								localStorage.setItem('user', user);
								$rootScope.currentUser = response.user;
								UserModel.init();
								$state.go('offers');
							})
							.error(function () {
								vm.loginError = true;
								vm.loginErrorMsg = error.data.error;
								console.log(vm.loginErrorMsg);
							})
					});
			}
		}
})();
