(function () {
	'use strict'

	angular
		.module('cmsApp')
		.controller('UsersCtrl', UsersCtrl);

	UsersCtrl.$inject = ['$http', '$auth', '$rootScope', '$q', '$state', 'UserModel', 'dataservice', '$uibModal', 'helpservice'];

	function UsersCtrl ($http, $auth, $rootScope, $q, $state, UserModel, dataservice, $uibModal, helpservice) {
		var vm = this;
		vm.users = UserModel.Collection.users;
		vm.user = new UserModel.Model();
		vm.helper = UserModel.Helpers;

		vm.edit = function (user) {
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/users/update/update-user-modal.html',
				controller: 'userUpdateCtrl',
				controllerAs: 'update',
				size: 'lg',
				resolve: {
					User: function () { return user; }
				}
			});

			modal.result.then(function (userToUpdate) {
				if (userToUpdate) {
					dataservice.update( 'users', userToUpdate, userToUpdate.id)
						.then(function (response) {
							vm.user = userToUpdate;
						})
						.catch(function (error) {
							console.log(error);
						});
				}
			});
		};

		vm.create = function () {
			// Modal Logic
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/users/create/create-user-modal.html',
				controller: 'userCreateCtrl',
				controllerAs: 'create',
				size: 'lg',
				resolve: {
					newUser: function () { return vm.user; }
				}
			});

			modal.result.then(function (userToCreate) {
				if (userToCreate) {
					if (vm.helper.canAddUser(vm.users, userToCreate.email)) {
						dataservice.create('users', userToCreate)
						.then(function (response) {
							console.log(response);
							userToCreate.id = response.data.newuser; // FIXME in controller TODO add error handling
							vm.users.push(userToCreate);
							vm.user = {};
							vm.user = new UserModel.Model();
						})
						.catch(function (error) {
							console.log(error);
						});
					}
					else
						console.log('User already exist!!');
				}
			});

		};

		vm.delete = function (user_id) {
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/users/delete/delete-user-modal.html',
				size: 'md',
				controller: 'userDeleteCtrl',
				controllerAs: 'delete',
				resolve: {
					idToDelete: function () { return user_id; }
				}
			});

			modal.result.then(function (willBeDeleted) {
				if (willBeDeleted.accept) {
					dataservice.delete('users', willBeDeleted.id)
						.then(function (response) {
							vm.users.splice(
								helpservice.getIndex(
									vm.users, 'id', willBeDeleted.id
								),
							1);
						})
						.catch(function (error) {
							console.log(error);
						});
				}
			});
		}

	}
})();
