(function () {
	'use strict'

	angular
		.module('cmsApp')
		.config(UsersConfig);

		UsersConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

		function UsersConfig ($stateProvider, $urlRouterProvider) {
			$stateProvider
				.state('users', {
					url: '/users',
					data: {
						permissions: {
							except: ['anonymous'],
							redirectTo: 'auth'
						}
					},
					templateUrl: '../app/components/tables/users/user-view.html',
					controller: 'UsersCtrl',
					controllerAs: 'user'
				});
		}
})();
