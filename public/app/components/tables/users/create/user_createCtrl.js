(function () {
	'use strict'
	angular
		.module('cmsApp')
		.controller('userCreateCtrl', userCreateCtrl);

	userCreateCtrl.$inject = ['$uibModalInstance', '$http', '$auth',
		'$rootScope', 'newUser', 'helpservice'
	];

	function userCreateCtrl ($uibModalInstance, $http, $auth, $rootScope, newUser, helpservice) {
		var mv = this;
		mv.user = newUser;
		var roles = helpservice.CONSTANTS.USER_ROLE;
		mv.dropdown = [
			{ role_id: roles.ADMINISTRATOR, name: 'Administrator'},
			{ role_id: roles.USER, name: 'User'}
		];
		mv.selected_role = undefined;
		mv.confirmationPassword = undefined;

		mv.submit = function () {
			mv.user.role_id = mv.selected_role.role_id;
			if (mv.user.password == mv.confirmationPassword) {
				$uibModalInstance.close(mv.user);
			}
			else
				alert("Password and Confirmation Password didn't match");
		};

		mv.cancel = function () {
			$uibModalInstance.close();
		};
	}
})();
