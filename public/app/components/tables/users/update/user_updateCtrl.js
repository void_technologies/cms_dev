(function () {
	'use strict'
	angular
		.module('cmsApp')
		.controller('userUpdateCtrl', userUpdateCtrl);

	userUpdateCtrl.$inject = ['$uibModalInstance', '$http', '$auth',
		'$rootScope', 'User', '$filter', 'helpservice'];

	function userUpdateCtrl ($uibModalInstance, $http, $auth,
		 $rootScope, User, $filter, helpservice) {
		var mv = this;
		mv.user = User;
		var roles = helpservice.CONSTANTS.USER_ROLE;
		mv.dropdown = [
			{ role_id: roles.ADMINISTRATOR, name: 'Administrator'},
			{ role_id: roles.USER, name: 'User'}
		];
		mv.selected_role = $filter('filter')(mv.dropdown, {
			role_id: mv.user.role_id
		}, true)[0];

		mv.submit = function () {
			mv.user.role_id = mv.selected_role.role_id;
			$uibModalInstance.close(mv.user);
		};

		mv.cancel = function () {
			$uibModalInstance.close();
		};
	}
})();
