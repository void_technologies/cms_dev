(function () {
	'use strict'

	angular
		.module('cmsApp')
		.controller('userDeleteCtrl', userDeleteCtrl);

	userDeleteCtrl.$inject = ['$uibModalInstance', '$http', '$auth',
		'$rootScope', 'idToDelete'
	];

	function userDeleteCtrl ($uibModalInstance, $http, $auth, $rootScope,
		 idToDelete ) {
			 var mv = this;
			 mv.willBeDeleted = {
				 accept: undefined,
				 id: idToDelete
			 };

			 mv.submit = function () {
				 mv.willBeDeleted.accept = true;
				 $uibModalInstance.close(mv.willBeDeleted);
			 };

			 mv.cancel = function () {
				 mv.willBeDeleted.accept = false;
				 $uibModalInstance.close( mv.willBeDeleted);
			 };
	 }
})();
