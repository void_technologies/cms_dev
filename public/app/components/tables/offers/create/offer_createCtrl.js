(function () {
	'use strict'

	angular
		.module('cmsApp')
		.controller('offerCreateCtrl', offerCreateCtrl);

	offerCreateCtrl.$inject = ['$uibModalInstance', '$http', '$auth',
		'$rootScope', 'newOffer', 'CategoryModel', 'helpservice'
	];

	function offerCreateCtrl ($uibModalInstance, $http, $auth,
		$rootScope, newOffer, CategoryModel, helpservice) {
			var mv = this;
			mv.offer = newOffer;
			mv.category_dd = helpservice.filter(
				CategoryModel.Collection.categories, "id", 1, false
			);
			mv.category_selected;

			mv.submit = function () {
				mv.offer.category_id = mv.category_selected.id;
				$uibModalInstance.close(mv.offer);
			};

			mv.cancel = function () {
				$uibModalInstance.close();
			};
	}

})();
