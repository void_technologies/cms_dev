(function () {
	'use strict'

	angular
		.module('cmsApp')
		.controller('offerUpdateCtrl', offerUpdateCtrl);

	offerUpdateCtrl.$inject = ['$uibModalInstance', '$http', '$auth',
		'$rootScope', 'Offer', 'helpservice', 'CategoryModel', 'dataservice','$scope'
	];

	function offerUpdateCtrl ($uibModalInstance, $http, $auth,
		$rootScope, Offer, helpservice, CategoryModel, dataservice, $scope) {
			var mv = this;
			mv.offer = Offer;
			mv.category_dd = helpservice.filter(
				CategoryModel.Collection.categories, 'id', 1, false
			);
			mv.category = helpservice.filter(
				mv.category_dd, 'id', mv.offer.category_id, true
			)[0];

			mv.isPublished = function (published) {
				console.log(mv.offer.published);
				return mv.offer.published = 1 - published;
			};

			mv.submit = function () {
				mv.offer.category_id = mv.category.id;
				$uibModalInstance.close(mv.offer);
			};

			mv.upload = function (event) {
				event.preventDefault();
				console.log(event.currentTarget);
				$.ajax({
					url: "api/photo",
					type: "POST",
					data: new FormData(event.currentTarget),
					contentType: false,
					cache: false,
					processData: false,
					success: function (response) {
						mv.offer.image = response.image_path+"offer01.jpg";
						$scope.$apply();
					},
					error: function (response) {
						console.log(response.error);
					}
				});
			};

			mv.cancel = function () {
				$uibModalInstance.close();
			};
	}

})();
