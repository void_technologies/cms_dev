(function () {
	'use strict'

	angular
		.module('cmsApp')
		.config(OffersConfig);

		OffersConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

		function OffersConfig ($stateProvider, $urlRouterProvider) {
			$stateProvider
				.state('offers', {
					url: '/offers',
					data: {
						permissions: {
							except: ['anonymous'],
							redirectTo: 'auth'
						}
					},
					templateUrl: '../app/components/tables/offers/offers-view.html',
					controller: 'OffersCtrl',
					controllerAs: 'offer'
				});
		}
})();
