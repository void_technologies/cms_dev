(function () {
	'use strict'
	angular
		.module('cmsApp')
		.controller('OffersCtrl', OffersCtrl);

	OffersCtrl.$inject = ['$http', '$auth', '$rootScope', '$q', '$state',
		'OfferModel', 'dataservice', '$uibModal', 'helpservice'
 	];

	function OffersCtrl ($http, $auth, $rootScope, $q, $state,
		 OfferModel, dataservice, $uibModal, helpservice) {
		var vm = this;
		vm.paginate = helpservice.pagination(OfferModel.Collection.offers, 10);
		vm.offer = new OfferModel.Model();

		vm.offers = vm.paginate["pages"][0];

		vm.edit = function (offer) {
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/offers/update/update-offer-modal.html',
				controller: 'offerUpdateCtrl',
				controllerAs: 'update',
				size: 'lg',
				resolve: {
					Offer: function () { return offer; }
				}
			});

			modal.result.then(function (offerToUpdate) {
				if (offerToUpdate) {
					console.log(offerToUpdate);
					dataservice.update( 'offers', offerToUpdate, offerToUpdate.id)
						.then(function (response) {
							vm.offer = {};
							vm.offer = new OfferModel.Model();
						});
				}
			})
		};

		vm.create = function () {
			// Modal Logic
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/offers/create/create-offer-modal.html',
				controller: 'offerCreateCtrl',
				controllerAs: 'create',
				size: 'lg',
				resolve: {
					newOffer: function () { return vm.offer; }
				}
			});

			modal.result.then(function (offerToCreate) {
				if (offerToCreate) {
					var request = dataservice.request(offerToCreate);
					dataservice.create( 'offers', request)
						.then(function (response) {
							offerToCreate.id = response.data.id;
							console.log(response.data, offerToCreate.id);
							vm.offers.push(offerToCreate);
							vm.offer = {};
							vm.offer = new OfferModel.Model();
						})
						.catch(function (error) {
							console.log(error.error);
						});
				}
			});

		};

		vm.delete = function (offer_id) {
			var modal = $uibModal.open({
				animation: true,
				templateUrl: '../app/components/tables/offers/delete/delete-offer-modal.html',
				size: 'md',
				controller: 'offerDeleteCtrl',
				controllerAs: 'delete',
				resolve: {
					idToDelete: function () { return offer_id; }
				}
			});

			modal.result.then(function (willBeDeleted) {
				if (willBeDeleted.accept) {
					dataservice.delete('offers', willBeDeleted.id)
						.then(function (response) {
							vm.offers.splice(
								helpservice.getIndex(
									vm.offers, 'id', willBeDeleted.id
								),
							1);
						});
				}
			});
		}
	}
})();
