(function () {
	'use strict'

	angular
		.module('cmsApp')
		.run(AppRun)
		.config(AppConfig);

	AppRun.$inject = ['$rootScope', '$state', '$auth', 'RoleStore', 'CategoryModel', 'OfferModel'];
	AppConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$authProvider'];

	function AppRun ($rootScope, $state, $auth, RoleStore, CategoryModel, OfferModel) {
		RoleStore
			.defineRole('anonymous', [], function (stateParams) {
				// If the returned value is 'truthy' then the user has the role,
				// otherwise they don't
				if (! $auth.isAuthenticated()) {
					return true;
				}
				return false;
			});

		RoleStore
			.defineRole('isloggedin', [], function (stateParams) {
				// If returned value is 'truthy' then the user has the role,
				// otherwise they don't
				if ($auth.isAuthenticated()) {
					return true; // Is logged in
				}
				return false;
			});

		$rootScope.logout = function () {
			$auth.logout()
				.then(function () {
					localStorage.removeItem('user');
					$rootScope.currentUser = null;
					$state.go('auth');
				});
		}
		$rootScope.currentUser = JSON.parse(localStorage.getItem('user'));

		OfferModel.init();
		CategoryModel.init();
	}

	function AppConfig ($stateProvider, $urlRouterProvider, $authProvider) {
		// satellizer configuration that specifies
		// which API route the JWT should be
		// retrieved from
		$authProvider.loginUrl = '/api/authenticate';
		// Redirect to auth state if any other states
		// are requested other than users
		$urlRouterProvider.otherwise('/auth');
	}

})();
