(function () {
	"use strict"

	angular
		.module('cmsApp')
		.factory('helpservice', helpservice);

	function helpservice () {
		return {
			getIndex   : getIndex,
			filter     : filter,
      		pagination : pagination,
			CONSTANTS: new CONSTANTS(),
		};

		/*************************************
			@desc Some Usefull Constants
		*************************************/
		function CONSTANTS () {
			this.USER_ROLE = {
				ADMINISTRATOR: 1,
				USER         : 2
			};
		}

		/*************************************
			@desc Array Helper Operations
		**************************************/

		function getIndex (array, key, value) {
			if (array.length < 100) {
				return linear_search(array, key, value);
			}
			else {
				return binary_search(array, key, value);
			}
		}

		function linear_search (array, key, value) {
			var index;
			for (var i = 0; i < array.length; i++)
				if (array[i][key] == value)
					return i;
			return -1;
		}

		function binary_search (array, key, value) {
			var min = 0;
			var max = array.length - 1;
			var current;
			var element;

			while (min <= max) {
				current = (min - max) / 2 | 0;
				element = array[current][key];

				if (element < value) {
					min = current + 1;
				}
				else if (element > value) {
					max = current - 1;
				}
				else {
					return current;
				}
			}
			return -1;
		}

		function filter (array, key, value, isEqual) {
			var newArray = [];
			if (isEqual) {
				newArray = filterByEquality(array, key, value);
			}
			else {
				newArray = filterByInequality(array, key, value);
			}
			return newArray;
		}

		function filterByEquality (array, key, value) {
			var filtered = [];
			for (var i = 0; i < array.length; i++) {
				if (array[i][key] == value) {
					filtered.push(array[i]);
				}
			}
			return filtered;
		}

		function filterByInequality (array, key, value) {
			var filtered = [];
			for (var i = 0; i < array.length; i++) {
				if (array[i][key] != value) {
					filtered.push(array[i]);
				}
			}
			return filtered;
		}

		function pagination (array, limit) {
			var paginationArray = [];
			paginationArray["pages"] = [[]];
			paginationArray["total_pages"] = Math.ceil(array.length/limit);
			var page = 1;

			for (var i = 1; i != array.length; i++) {
				if (i % (limit + 1) != 0) {
				  paginationArray["pages"][page - 1].push(array[i - 1]);
				}
				else {
				  page += 1;
				  paginationArray["pages"][page - 1] = [array[i - 1]];
				}
			}
			return paginationArray;
		}
	}
})();
