(function () {
	'use strict'

	angular
		.module('cmsApp')
		.factory('dataservice', dataservice);

	dataservice.$inject = ['$http','$auth', '$rootScope', '$q', '$state'];

	function dataservice ($http, $auth, $rootScope, $q, $state) {
		var api = this;
		var url = 'api/';
		return {
			all   : all,
			create: create,
			update: update,
			delete: destroy,
			show  : show,

			// Helpers
			request : preparedData,
		};

		/*****************************************
		 *                CRUD                   *
		 *****************************************/
		 function all (param) {
			 return $http.get(url+param)
			 	.then(getResponse)
				.catch(getError);
		 }

		 function create (url_param, data) {
			 return $http.post(url+url_param, data)
			 	.then(getResponse)
				.catch(getError);
		 }

		 function update (url_param, data, id) {
			 return $http.put(url+url_param+'/'+id, data)
				 .then(getResponse)
				 .catch(getError);
		 }

		 function destroy (url_param, id) {
			 return $http.delete(url+url_param+'/'+id)
			 	.then(getResponse)
				.catch(getError);
		 }

		 function show (url_param, id) {
			 return $http.get(url+url_param+'/'+id)
			 	.then(getResponse)
				.cacth(getError);
		 }

		/*****************************************
		 *             Helpers Method            *
		 *****************************************/
		 function getResponse (response) {
			 if (response) {
				 return response;
			 }
		 }

		 function getError (error) {
			 if (error) {
				 return error;
			 }
		 }

		 function preparedData (model) {
			 var data = {};
			 if (model) {
				 data["data"] = model;
			 }
			 return data;
		 }
	}
})();
